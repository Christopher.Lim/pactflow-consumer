const { Publisher } = require("@pact-foundation/pact");
const project = require('./package.json')
const opts = {
  pactBroker: process.env.PACT_BROKER_BASE_URL,
  pactBrokerToken: process.env.PACT_BROKER_TOKEN,
  consumerVersion: process.env.CI_COMMIT_SHORT_SHA + project.version,
  pactFilesOrDirs: ['./contract/pacts'],
  tags: [process.env.CI_COMMIT_REF_NAME]
};

new Publisher(opts).publishPacts()